import React, { Component } from 'react';
import './App.css';

import SignIn from './SignIn';
import RaisedButton from 'material-ui/RaisedButton';
import { withNamespaces } from 'react-i18next';

import i18n from 'i18next';

class App extends Component {
  constructor(props){
    super(props);
    this.state={
      currentScreen:[]
    }
  }
  
  componentWillMount(){
    var loginPage =[];
    loginPage.push(<SignIn appContext={this} key={"login-screen"}/>);
    this.setState({currentScreen:loginPage})
  }
  
  render() {
    const changeLanguage = (lng) => {
	    i18n.changeLanguage(lng);
	}
	  
    return (
      <div className="App">
        {this.state.currentScreen}
      </div>
    );
  }
}

export default withNamespaces()(App);
