import React from 'react';

export const language = {
  ru: "ru",
  en: "en",
};

export const ThemeContext = React.createContext(  language.en );

