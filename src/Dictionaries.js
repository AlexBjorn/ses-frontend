import React, { Component } from 'react';

import PersonalArea from './PersonalArea';
import EducationLevels from './dictionaries/EducationLevels';
import SkillTypes from './dictionaries/SkillTypes';
import Projects from './dictionaries/Projects';
import Roles from './dictionaries/Roles';
import Skills from './dictionaries/Skills';
import RoleSkillTypes from './dictionaries/RoleSkillTypes';
import ValuationPeriods from './dictionaries/ValuationPeriods';
import i18n from 'i18next';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ReplyIcon from '@material-ui/icons/Reply';

class Dictionaries extends Component {
  constructor(props){
    super(props);
    this.state={
      role:'user',
    }
    this.handleEducationLevelsClick=this.handleEducationLevelsClick.bind(this);
    this.handleProjectsClick=this.handleProjectsClick.bind(this);
    this.handleRolesClick=this.handleRolesClick.bind(this);
    this.handleSkillsClick=this.handleSkillsClick.bind(this);
    this.handleSkillTypesClick=this.handleSkillTypesClick.bind(this);
    this.handleRoleSkillTypesClick=this.handleRoleSkillTypesClick.bind(this);
    this.handleValuationPeriodsClick=this.handleValuationPeriodsClick.bind(this);
    
    this.handleReturnClick=this.handleReturnClick.bind(this);
  }
  
  componentWillMount(){
    this.setState({role:this.props.role});
  }
    
  handleEducationLevelsClick(event) {
	console.log("handleEducationLevelsClick");  
    var educationLevels = [];
    educationLevels.push(<EducationLevels appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:educationLevels});
  }

  handleProjectsClick(event) {
    var projects = [];
    projects.push(<Projects appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:projects});
  }

  handleRolesClick(event) {
    var roles = [];
    roles.push(<Roles appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:roles});
  }
  
  handleSkillsClick(event) {
    var skills = [];
    skills.push(<Skills appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:skills});
  }

  handleSkillTypesClick(event) {
    var skillTypes = [];
    skillTypes.push(<SkillTypes appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:skillTypes});
  }
  
  handleRoleSkillTypesClick(event) {
    var roleSkillTypes = [];
    roleSkillTypes.push(<RoleSkillTypes appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:roleSkillTypes});
  }
  
  handleValuationPeriodsClick(event) {
    var valuationPeriods = [];
    valuationPeriods.push(<ValuationPeriods appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:valuationPeriods});
  }
  
  handleReturnClick(event){
	    var self = this;
	    var personalArea = [];
	    personalArea.push(<PersonalArea appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:personalArea});
  } 

render() {
    return (
    	<Container component="main" >
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>           
    				<Grid item xs={12}>
    					<AppBar position="static">
    						<Toolbar>
    							<IconButton edge="start" color="inherit" aria-label="menu">
    								<MenuIcon />
    							</IconButton>
    							<Typography variant="h6">
    								{i18n.t("dictionaties.title")}
    							</Typography>
    							<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleReturnClick(event)}>
									<ReplyIcon />
								</IconButton>
    						</Toolbar>
    					</AppBar>
    				</Grid>
    				
            		<Grid item xs={4}>
            			<Button
                        	type="submit"
                        	fullWidth
                        	variant="contained"
                        	color="primary"
                        	onClick={(event) => this.handleEducationLevelsClick(event)}
            			>
            				{i18n.t("education.level.dict.button.label")}
            			</Button>
                    </Grid>
                    
                    <Grid item xs={4}>
                      	<Button
                      		type="submit"
                      		fullWidth
                      		variant="contained"
                      		color="primary"
                      		onClick={(event) => this.handleProjectsClick(event)}
                      	>
                      		{i18n.t("project.dict.button.label")}
                      	</Button>
                    </Grid>
                    
                    <Grid item xs={4}>
                    	<Button
                    		type="submit"
                    		fullWidth
                    		variant="contained"
                    		color="primary"  
                    		onClick={(event) => this.handleRolesClick(event)}
                    	>
                    		{i18n.t("role.dict.button.label")}
                    	</Button>
                    </Grid>
                    
	                <Grid item xs={4}>
	                  	<Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"
	                  		onClick={(event) => this.handleSkillsClick(event)}
	                  	>
	                  		{i18n.t("skill.dict.button.label")}
	                  	</Button>           
	                </Grid>
	                	                
	                <Grid item xs={4}>
	                  	<Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"
	                  		onClick={(event) => this.handleRoleSkillTypesClick(event)}
	                  	>
	                  		{i18n.t("role.skill.type.dict.button.label")}
	                  	</Button>           
	                </Grid>
	                
	                <Grid item xs={4}>
	                  	<Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"
	                  		onClick={(event) => this.handleValuationPeriodsClick(event)}
	                  	>
	                  		{i18n.t("valuation.period.dict.button.label")}
	                  	</Button>           
	                </Grid>

	           </Grid>
	       </div>
      </Container>	
    		
    );
  }
}

export default Dictionaries;