import React, { Component } from 'react';

import Profile from './Profile';
import Resume from './Resume';
import Dictionaries from './Dictionaries';
import Valuation360 from './Valuation360';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import i18n from 'i18next';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
 
const style = {
		margin: 15,
};

const useStyles = makeStyles((theme) => ({
	  paper: {
	    marginTop: theme.spacing(8),
	    display: 'flex',
	    flexDirection: 'column',
	    alignItems: 'center',
	  },
	  avatar: {
	    margin: theme.spacing(1),
	    backgroundColor: theme.palette.secondary.main,
	  },
	  form: {
	    width: '100%', // Fix IE 11 issue.
	    marginTop: theme.spacing(3),
	  },
	  submit: {
	    margin: theme.spacing(3, 0, 2),
	  },
	}));

class PersonalArea extends Component {
  constructor(props){
    super(props);
    this.state={
      role:'user',
    }
    this.handleProfileClick=this.handleProfileClick.bind(this);
    this.handleResumeClick=this.handleResumeClick.bind(this);
    this.handleDictionariesClick=this.handleDictionariesClick.bind(this);
    this.handleValuation360Click=this.handleValuation360Click.bind(this);
  }
  
  componentWillMount(){
    this.setState({role:this.props.role});
  }
    
handleProfileClick(event) {
    var profile = [];
    profile.push(<Profile appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:profile});
}

handleResumeClick(event) {
    var resume = [];
    resume.push(<Resume appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:resume});
}

handleDictionariesClick(event) {
    var dictionaries = [];
    dictionaries.push(<Dictionaries appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user}/>);
    this.props.appContext.setState({currentScreen:dictionaries});
}

handleValuation360Click(event) {
    var valuation360 = [];
    var nullPeriod = {id: null, name: '', startDate: null, finishDate: null};
    valuation360.push(<Valuation360 appContext={this.props.appContext} role={this.state.loginRole} user={this.props.user} period={nullPeriod}/>);
    this.props.appContext.setState({currentScreen:valuation360});
}

render() {
	const { classes } = this.props;
    return (
    	<Container component="main" >
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>           
    				<Grid item xs={12}>
    					<AppBar position="static">
    						<Toolbar>
    							<IconButton edge="start" color="inherit" aria-label="menu">
    								<MenuIcon />
    							</IconButton>
    							<Typography variant="h6">
    								{i18n.t("personal.area.title")}
    							</Typography>
    						</Toolbar>
    					</AppBar>
    				</Grid>
    		
            		<Grid item xs={3}>
            			<Button
                        	type="submit"
                        	fullWidth
                        	variant="contained"
                        	color="primary"
                        	onClick={(event) => this.handleProfileClick(event)}
            			>
            				{i18n.t("profile.button.label")}
            			</Button>
                    </Grid>
                    <Grid item xs={3}>
                      	<Button
                      		type="submit"
                      		fullWidth
                      		variant="contained"
                      		color="primary"
                      		onClick={(event) => this.handleResumeClick(event)}
                      	>
                      		{i18n.t("resume.button.label")}
                      	</Button>
                    </Grid>
                    <Grid item xs={3}>
                    	<Button
                    		type="submit"
                    		fullWidth
                    		variant="contained"
                    		color="primary"  
                    		onClick={(event) => this.handleDictionariesClick(event)}
                    	>
                    		{i18n.t("dictionaries.button.label")}
                    	</Button>
                    </Grid>
	                <Grid item xs={3}>
	                  	<Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"
	                  		onClick={(event) => this.handleValuation360Click(event)}
	                  	>
	                  		{i18n.t("valuation360.button.label")}
	                  	</Button>           
	                </Grid>
	           </Grid>
	       </div>
      </Container>
    );
  }
}

export default PersonalArea;