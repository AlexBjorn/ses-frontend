import React, { Component } from 'react';
import axios from 'axios';
import PersonalArea from './PersonalArea';
import i18n from 'i18next';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

const style = {
  margin: 15,
};

class Profile extends Component {
  
  constructor(props){
    super(props);
    this.state={
	      login:props.user.login,
	      password:props.user.password,
	      repeatePassword:props.user.password,
	      snackbar: {openflg: false, message: '',	severity: 'success'}
    }
    
    this.handleSaveClick=this.handleSaveClick.bind(this);
    this.handleReturnClick=this.handleReturnClick.bind(this);
    this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
  }
  
  handleSaveClick(event) {
	  if(this.state.password==this.state.repeatePassword) {
		  var user=this.props.user;
		  user.password=this.state.password;
	
		   axios.post(process.env.REACT_APP_API_URL+'/register/changepswd', this.props.user)
		   .then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
	  } else {
		  this.setState({snackbar:{
				openflg: true, 
				message: i18n.t("passwords.notequals.warn.message"), 
				severity: "warning"}});
	  }
  }
  
  handleReturnClick(event){
	    var self = this;
	    var personalArea = [];
	    personalArea.push(<PersonalArea appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:personalArea});
  }  
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
    
  render() {

    return (
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>           
    				<Grid item xs={12}>
    					<AppBar position="static">
    						<Toolbar>
    							<IconButton edge="start" color="inherit" aria-label="menu">
    								<MenuIcon />
    							</IconButton>
    							<Typography variant="h6">
    								{i18n.t("profile.title")}
    							</Typography>
    						</Toolbar>
    					</AppBar>
    				</Grid>
    				<Grid item xs={12}>
    					<Container  maxWidth="xs">
    					<Grid container spacing={2}>
    					<Grid item xs={12}>
	    					<TextField
	    		            variant="outlined"
	    		            margin="normal"
	    		            required
	    		            fullWidth
	    		            id="login"
	    		            label={i18n.t("login.label")}
	    		            name="login"
	    		            autoFocus
	    		            value={this.props.user.login}
	    		            onChange={(event) => this.setState({login:event.target.value})}
	    		          />
	    				</Grid>  
	    				<Grid item xs={12}>
	    				  <TextField
	    		            variant="outlined"
	    		            margin="normal"
	    		            required
	    		            fullWidth
	    		            name="password"
	    		            label={i18n.t("password.label")}
	    		            type="password"
	    		            id="password"
	    		            value={this.state.password}
	    		            onChange = {(event) => this.setState({password:event.target.value})}
	    		          />
	    				  </Grid>  
		    				<Grid item xs={12}>
	    				  <TextField
	    		            variant="outlined"
	    		            margin="normal"
	    		            required
	    		            fullWidth
	    		            name="repeatePassword"
	    		            label={i18n.t("repeate.password.label")}
	    		            type="password"
	    		            id="password"
	    		            value={this.state.repeatePassword}
	    		            onChange = {(event) => this.setState({password:event.target.value})}
	    		          />
	    				</Grid>  
	    				<Grid item xs={12}>	
	    				  <Button
                    		type="submit"
                    		fullWidth
                    		variant="contained"
                    		color="primary"  
                    		onClick={(event) => this.handleSaveClick(event, this.props.role)}
                    	  >
	    					{i18n.t("submit.button.label")}
                    	  </Button>
                    	  </Grid>  
  	    				<Grid item xs={12}>	
                    	  <Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"  
	                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
	                  	  >
		    					{i18n.t("return.button.label")}
	                  	  </Button>
	    		          
    					</Grid>
    					</Grid>
    					</Container>
    				</Grid>
    			</Grid>
            		
    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
			        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
			        	{this.state.snackbar.message}
			        </Alert>
			    </Snackbar>
           
      </div>
      </Container>
    );
  }
}

export default Profile;
