import React, { Component } from 'react';
import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import AppBar from 'material-ui/AppBar';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
import axios from 'axios';
import Login from './Login';
import i18n from 'i18next';


class Register extends Component {
  
  constructor(props){
    super(props);
    this.state={
      login:'',
      email:'',
      password:'',
      repeatePassword:''
    }
  }
  
  componentWillReceiveProps(nextProps){
    console.log("nextProps",nextProps);
  }
  
  handleClick(event,role){
    // console.log("values in register handler",role);
    var self = this;
    //To be done:check for empty values before hitting submit
    if(this.state.login.length>0 && this.state.password.length>0 && this.state.email.length>0 && this.state.repeatePassword.length>0){
      var registerUrl = process.env.REACT_APP_API_URL+'/register';
      var payload={
	      "email": this.state.email,
	      "login":this.state.login,
	      "password":this.state.password,
	      "repeatePassword":this.state.repeatePassword
      }
      axios.post(registerUrl, payload)
     .then(function (response) {
       console.log(response);
       if(response.data.code === 200){
        //  console.log("registration successfull");
         var loginscreen=[];
         loginscreen.push(<Login parentContext={this} appContext={self.props.appContext} role={role}/>);
         var loginmessage = <label>{i18n.t("not.registered.message")}</label>;
         self.props.parentContext.setState({
        	 loginscreen:loginscreen,
        	 loginmessage:loginmessage,
        	 buttonLabel:<label>{i18n.t("register.button.label")}</label>,
        	 isLogin:true
          });
       }
       else{
         console.log("some error ocurred",response.data.code);
       }
     })
     .catch(function (error) {
       console.log(error);
     });
    }
    else{
      alert("Input field value is missing");
    }

  }
  
  render() {
    // console.log("props",this.props);
    var userhintText,userLabel;
    userhintText=<label>{i18n.t("login.hintText")}</label>;
    userLabel=<label>{i18n.t("login.label")}</label>;
    return (
      <div>
        <MuiThemeProvider>
          <div>
          <AppBar
             title={i18n.t("register.title")}
           />
           <TextField
             hintText={userhintText}
             floatingLabelText={userLabel}
             onChange = {(event,newValue) => this.setState({login:newValue})}
           />
           <br/>
           <TextField
           	 hintText="Enter your Email"
             floatingLabelText="Email"
             onChange = {(event,newValue) => this.setState({email:newValue})}
            />
           <br/>
           <TextField
             type = "password"
             hintText={i18n.t("password.hintText")}
             floatingLabelText={i18n.t("password.label")}
             onChange = {(event,newValue) => this.setState({password:newValue})}
             />
           <br/>
           <TextField
           	 type = "password"
           	 hintText={i18n.t("repeate.password.hintText")}
             floatingLabelText={i18n.t("repeate.password.label")}
           	 onChange = {(event,newValue) => this.setState({repeatePassword:newValue})}
            />
           <br/>
           <RaisedButton label={i18n.t("submit.button.label")} primary={true} style={style} onClick={(event) => this.handleClick(event,this.props.role)}/>
          </div>
         </MuiThemeProvider>
      </div>
    );
  }
}

const style = {
  margin: 15,
};

export default Register;
