import React, { Component } from 'react';

import axios from 'axios';
import PersonalArea from './PersonalArea';

import { makeStyles } from '@material-ui/core/styles';
import i18n from 'i18next';
import Employee from './resume/Employee';
import EmployeeProjects from './resume/EmployeeProjects';
import EmployeeExperience from './resume/EmployeeExperience';
import EmployeeEducations from './resume/EmployeeEducations';

import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

const style = {
  margin: 15,
};



class Resume extends Component {
	
  constructor(props){
    super(props);
    this.state={
	      employee:props.user.employee,
	      employeeProjects:null,
	      employeeEducations:null,
	      employeeExperiences:null,
	      snackbar: {openflg: false, message: '', severity: 'success'}
    }
    
    this.handleEmployeeChange=this.handleEmployeeChange.bind(this);
    this.handleEmployeeProjectsChange=this.handleEmployeeProjectsChange.bind(this);
    this.handleEmployeeExperiencesChange=this.handleEmployeeExperiencesChange.bind(this);
    this.handleEmployeeEducationsChange=this.handleEmployeeEducationsChange.bind(this);
    this.handleSaveClick=this.handleSaveClick.bind(this);
    this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
  }
  
  handleSaveClick(event) {
	  var employeeInfo={
			  employee: this.state.employee,
			  employeeProjects: this.state.employeeProjects,
			  employeeExperience: this.state.employeeExperiences,
			  employeeEducation: this.state.employeeEducations
	  };
	  
	  var url=process.env.REACT_APP_API_URL+'/employee/change';
	  axios.post(url, employeeInfo)
	   .then((response) => {
	     console.log(response);
	     if(response.status == 200){
				this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.success.message"), 
					severity: "success"
				}});
			} else {
				this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.error.message"), 
					severity: "error"}});
			}
	   })
	   .catch((error) => {
	     console.log(error);
	     this.setState({snackbar:{
				openflg: true, 
				message: i18n.t("save.error.message"), 
				severity: "error"}});
	   });  
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
    
  handleReturnClick(event){
	    var self = this;
	    var personalArea = [];
	    personalArea.push(<PersonalArea appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:personalArea});
	  }  
  
  handleEmployeeChange(changedEmployee) {
	  this.setState({employee:changedEmployee});
  }
  
  handleEmployeeProjectsChange(changedEmployeeProjects) {
	  this.setState({employeeProjects:changedEmployeeProjects});
  }
  
  handleEmployeeExperiencesChange(changedEmployeeExperiences) {
	  this.setState({employeeExperiences:changedEmployeeExperiences});
  }
  
  handleEmployeeEducationsChange(changedEmployeeEducations) {
	  this.setState({employeeEducations:changedEmployeeEducations});
  }
    
  render() {
    return (
    		<Container component="main" >
	    		<CssBaseline />
	    		<div>
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="fixed">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("resume.title")}
	    							</Typography>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
		    				<ExpansionPanel>
					          <ExpansionPanelSummary
					            expandIcon={<ExpandMoreIcon />}
					            aria-controls="panel1a-content"
					            id="employee-header"
					          >
					            <Typography>
					            	Общая информация
					            </Typography>
					          </ExpansionPanelSummary>
					          <ExpansionPanelDetails>
					            
					          	<Employee employee={this.state.employee} onChange={this.handleEmployeeExperiencesChange}/>
					           
					          </ExpansionPanelDetails>
				        </ExpansionPanel>
            				
            			</Grid>
	    		
	            		<Grid item xs={12}>
					        <ExpansionPanel>
						          <ExpansionPanelSummary
						            expandIcon={<ExpandMoreIcon />}
						            aria-controls="panel1a-content"
						            id="employee-projects-header"
						          >
						            <Typography>
						            	Проекты
						            </Typography>
						          </ExpansionPanelSummary>
						          <ExpansionPanelDetails>
						            
						            	<EmployeeProjects employee={this.state.employee} onChange={this.handleEmployeeProjectsChange}/>
						           
						          </ExpansionPanelDetails>
					        </ExpansionPanel>
           
				        </Grid>
			    		
				        <Grid item xs={12}>
					        <ExpansionPanel>
						          <ExpansionPanelSummary
						            expandIcon={<ExpandMoreIcon />}
						            aria-controls="panel1a-content"
						            id="employee-experiences-header"
						          >
						            <Typography>
						            	Опыт работы
						            </Typography>
						          </ExpansionPanelSummary>
						          <ExpansionPanelDetails>
						            
						          		<EmployeeExperience employee={this.state.employee} onChange={this.handleEmployeeExperiencesChange}/>
						           
						          </ExpansionPanelDetails>
					        </ExpansionPanel>
	       
				        </Grid>
	            		<Grid item xs={12}>
			            	<ExpansionPanel>
						          <ExpansionPanelSummary
						            expandIcon={<ExpandMoreIcon />}
						            aria-controls="panel1a-content"
						            id="employee-educations-header"
						          >
						            <Typography>
						            	Образование
						            </Typography>
						          </ExpansionPanelSummary>
						          <ExpansionPanelDetails>
						            
						          		<EmployeeEducations employee={this.state.employee} onChange={this.handleEmployeeExperiencesChange}/>
						           
						          </ExpansionPanelDetails>
					        </ExpansionPanel>
	            		</Grid>
				    		
					        
					        <Grid item xs={3}>	
		    				</Grid>  
		    				
						    <Grid item xs={3}>
						           
							  <Button
		                		type="submit"
		                		fullWidth
		                		variant="contained"
		                		color="primary"  
		                		onClick={(event) => this.handleSaveClick(event, this.props.role)}
		                	  >
		    					{i18n.t("submit.button.label")}
		                	  </Button>
		                	  </Grid>  
			    				<Grid item xs={3}>	
		                	  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
		                  	  >
			    					{i18n.t("return.button.label")}
		                  	  </Button>
						    
						    </Grid>
						    
		    				<Grid item xs={3}>	
		    				</Grid>  
					 </Grid>
				       <Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
					        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
					        	{this.state.snackbar.message}
					        </Alert>
			           </Snackbar>       
					           
				</div>
         </Container>
    );
  }
}

export default Resume;
