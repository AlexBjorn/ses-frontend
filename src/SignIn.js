import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import axios from 'axios';
import PersonalArea from './PersonalArea';
import SignUp from './SignUp';
import i18n from 'i18next';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = theme => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(1),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
});

class SignIn extends Component {
    
  constructor(props){
    super(props);
    this.state={
      username:'',
      password:'',
      loginRole:'user',
	  snackbar: {openflg: false, message: '', severity: 'success'}
    }
    this.handleClick=this.handleClick.bind(this);
    this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
    this.handleRegisterClick=this.handleRegisterClick.bind(this);
  }
  
  handleClick(event){
		var url = process.env.REACT_APP_API_URL+'/login';
	    var self = this;
	    var payload={
	      "login":this.state.username,
		  "password":this.state.password
	    }
	    	    
	   axios.post(url, payload)
	   .then((response) => {
				console.log(response);
				if(response.status == 200 && response.data.login != null){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("signin.greetings.message"), 
						severity: "success"
					}});
					var personalArea = [];
				    personalArea.push(<PersonalArea appContext={self.props.appContext} role={self.state.loginRole} user={response.data}/>);
				    self.props.appContext.setState({currentScreen:personalArea});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("signin.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("signin.error.message"), 
						severity: "error"}});
		});
	   
	  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
  
  handleRegisterClick(event) {
	  let registerscreen=[];
	  registerscreen.push(<SignUp parentContext={this} appContext={this.props.appContext} role={"user"}/>);
      this.props.appContext.setState({currentScreen:registerscreen});
  }

  render () {
	  const { classes } = this.props;
  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
        	{i18n.t("login.title")}
        </Typography>
        
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            id="login"
            label={i18n.t("login.label")}
            name="login"
            autoFocus
            onChange={(event) => this.setState({username:event.target.value})}
          />
          <TextField
            variant="outlined"
            margin="normal"
            required
            fullWidth
            name="password"
            label={i18n.t("password.label")}
            type="password"
            id="password"
            onChange = {(event) => this.setState({password:event.target.value})}
          />
          
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          	onClick={(event) => this.handleClick(event)}
          >
          	{i18n.t("enter.button.label")}
          </Button>
          <Grid container>
            <Grid item xs>
              <Link href="#" variant="body2">
              	{i18n.t("forgot.password.message")}
              </Link>
            </Grid>
            <Grid item>
              <Link href="#" variant="body2" onClick={this.handleRegisterClick}>
                {i18n.t("not.registered.message")}
              </Link>
            </Grid>
          </Grid>
        
      </div>
      <Box mt={8}>
        <Copyright />
      </Box>
      <Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
	    <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
	    	{this.state.snackbar.message}
	    </Alert>
	   </Snackbar>
    </Container>
    
  );
  }
}

export default withStyles(useStyles)(SignIn);