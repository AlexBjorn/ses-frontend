import React, { Component } from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Link from '@material-ui/core/Link';
import Grid from '@material-ui/core/Grid';
import Box from '@material-ui/core/Box';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import { makeStyles, withStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import axios from 'axios';
import SignIn from './SignIn';
import i18n from 'i18next';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

function Copyright() {
  return (
    <Typography variant="body2" color="textSecondary" align="center">
      {'Copyright © '}
      <Link color="inherit" href="https://material-ui.com/">
        Your Website
      </Link>{' '}
      {new Date().getFullYear()}
      {'.'}
    </Typography>
  );
}

const useStyles = makeStyles((theme) => ({
  paper: {
    marginTop: theme.spacing(8),
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'center',
  },
  avatar: {
    margin: theme.spacing(1),
    backgroundColor: theme.palette.secondary.main,
  },
  form: {
    width: '100%', // Fix IE 11 issue.
    marginTop: theme.spacing(3),
  },
  submit: {
    margin: theme.spacing(3, 0, 2),
  },
}));

class SignUp extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      login:'',
	      email:'',
	      password:'',
	      repeatePassword:'',
		  snackbar: {openflg: false, message: '', severity: 'success'}
	    }
	    
	    this.handleClick=this.handleClick.bind(this);
	    this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
	    this.handleLoginClick=this.handleLoginClick.bind(this);
	}
	
	handleClick(event,role){
	    var self = this;
	    if(this.state.login.length>0 && this.state.password.length>0 
	    		&& this.state.email.length>0 && this.state.repeatePassword.length>0){
	      var url = process.env.REACT_APP_API_URL+'/register';
	      var info={
		      "email": this.state.email,
		      "login":this.state.login,
		      "password":this.state.password,
		      "repeatePassword":this.state.repeatePassword
	      }
	      axios.post(url, info)
	      .then((response) => {
				console.log(response);
				if(response.status == 200 && response.data.login != null){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("registration.success.message"), 
						severity: "success"
					}});
					var loginscreen=[];
			        loginscreen.push(<SignIn parentContext={this} appContext={self.props.appContext} role={role}/>);
			        self.props.appContext.setState({currentScreen:loginscreen});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("registration.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("registration.error.message"), 
						severity: "error"}});
		});
	    } else {
	    	this.setState({snackbar:{
				openflg: true, 
				message: i18n.t("registration.not.entered.info.message"), 
				severity: "error"}});
	    }
	}
	
	handleSnackBarClose() {
		  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
	}

	handleLoginClick(event) {
		var loginscreen=[];
        loginscreen.push(<SignIn parentContext={this} appContext={this.props.appContext} role={"user"}/>);
        this.props.appContext.setState({currentScreen:loginscreen});
	}
	
	render() {
		const { classes } = this.props;

  return (
    <Container component="main" maxWidth="xs">
      <CssBaseline />
      <div className={classes.paper}>
        <Avatar className={classes.avatar}>
          <LockOutlinedIcon />
        </Avatar>
        <Typography component="h1" variant="h5">
        	{i18n.t("register.title")}
        </Typography>
        
          <Grid container spacing={2}>
            
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="login"
                label={i18n.t("login.label")}
                name="login"
                autoComplete="login"
                onChange = {(event) => this.setState({login:event.target.value})}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                id="email"
                label={i18n.t("email.label")}
                name="email"
                autoComplete="email"
                onChange = {(event) => this.setState({email:event.target.value})}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="password"
                label={i18n.t("password.label")}
                type="password"
                id="password"
                autoComplete="current-password"
                onChange = {(event) => this.setState({password:event.target.value})}
              />
            </Grid>
            <Grid item xs={12}>
              <TextField
                variant="outlined"
                required
                fullWidth
                name="repeatePassword"
                label={i18n.t("repeate.password.label")}
                type="password"
                id="repeatePassword"
                autoComplete="current-password"
                onChange = {(event) => this.setState({repeatePassword:event.target.value})}
              />
            </Grid>
            <Grid item xs={12}>
              
            </Grid>
          </Grid>
          <Button
            type="submit"
            fullWidth
            variant="contained"
            color="primary"
            className={classes.submit}
          	onClick={(event) => this.handleClick(event,this.props.role)}
          >
          		{i18n.t("submit.button.label")}
          </Button>
          <Grid container justify="flex-end">
            <Grid item>
              <Link href="#" variant="body2" onClick={this.handleLoginClick}>
              		{i18n.t("already.registered.message")}
              </Link>
            </Grid>
          </Grid>
        
      </div>
      <Box mt={5}>
        <Copyright />
      </Box>
    </Container>
  );
  }
}
              

export default withStyles(useStyles)(SignUp);