import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

import Valuation360 from './Valuation360';


class ValuateEmployee extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				period: props.period,
				currentEmployee: props.user.employee,
				valuatedEmployee: props.valuatedEmployee,
				valuations: [],
				snackbar: {openflg: false, message: '',	severity: 'success'}
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleValuationChange=this.handleValuationChange.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
	}
  
	componentDidMount() {
		var url = process.env.REACT_APP_API_URL 
			+ '/valuation/period/' + this.state.period.id 
			+ '/valuator/' + this.state.currentEmployee.id 
			+ '/employee/' + this.state.valuatedEmployee.id;
		fetch(url)
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({valuations: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {
			var info = {
					valuations: this.state.valuations
			};
			
			axios.post(process.env.REACT_APP_API_URL+'/valuation/change', info)
				.then((response) => {
					console.log(response);
					if(response.status == 200){
						this.setState({snackbar:{
							openflg: true, 
							message: i18n.t("save.success.message"), 
							severity: "success"
						}});
					} else {
						this.setState({snackbar:{
							openflg: true, 
							message: i18n.t("save.error.message"), 
							severity: "error"}});
					}
			})
		   .catch((error) => {
		     console.log(error);
		     this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.error.message"), 
					severity: "error"}});
		   });
	}
  
  handleReturnClick(event){
	    var self = this;
	    var valuation360 = [];
	    valuation360.push(<Valuation360 appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user} period={self.state.period}/>);
	    self.props.appContext.setState({currentScreen:valuation360});
  } 
  
  handleValuationChange(event, valuation) {
	  var vals = [];
	  vals = this.state.valuations.slice();
	  for(let i=0; i<vals.length; i++) {
		  if(vals[i].id == valuation.id) {
			  vals[i].valuation = event.target.value;
			  this.setState({valuations:vals});
			  break;
		  }
	  }
	  
	  
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
    
  render() {
	  return (		  	
		
			  <Container component="main">
	    		<CssBaseline />
	    		<div>
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="static">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("valuate.employee.title") + this.state.period.name}
	    							</Typography>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				
	    				<Grid item xs={12}>
	    					
		    					{
				    				this.state.valuations.map(item => (
				    						<Container  maxWidth="xs">
					    					<Grid container spacing={2}>
				    						<Grid item xs={8}>
						    					
						    					<TextField 
													id={"skillNameTextField"+item.id}
					    			   				label={item.skill.type.name} 
					    			   				defaultValue={item.skill.name} 
						    			   			InputProps={{
						    			   				readOnly: true,
						    			   			}}
												/>
											
						    				</Grid>	
						    				<Grid item xs={4}>
							    				<TextField
										          id={"valuationTextField"+item.id}
										          label="Оценка"
										          defaultValue={item.valuation}
										          type="number"
										          InputLabelProps={{
										            shrink: true,
										          }}
												  onChange={(event => this.handleValuationChange(event, item))}
										        />
						    				</Grid>  
						    				</Grid> 
						    				</Container>
				    				))
				    			}
			    				
		    			</Grid>  
			    		
	  	    				<Grid item xs={3}>	
	  	    				</Grid>	
		    				<Grid item xs={3}>
	  	    				  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleSaveClick(event, this.props.role)}
		                  	  >
	  	    				  	{i18n.t("save.button.label")}
		                  	  </Button>	
		                  	</Grid>	
		    				<Grid item xs={3}>
	                    	  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
		                  	  >
			    					{i18n.t("return.button.label")}
		                  	  </Button>
		                  	</Grid>	
		    				<Grid item xs={3}>
	    					</Grid>
	    					
	    			
	    			</Grid>
	            		
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container>  
				
    );
  }
}

export default ValuateEmployee;
