import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import EqualizerIcon from '@material-ui/icons/Equalizer';
import Box from '@material-ui/core/Box';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

import ValuateEmployee from './ValuateEmployee';
import PersonalArea from './PersonalArea';
import ValuationPeriodSelect from './select/ValuationPeriodSelect';

class Valuation360 extends Component {
	
  constructor(props){
    super(props);
    this.state={
      employee:props.user.employee,
      period: props.period,
      coEmployees: [],
      snackbar: {openflg: false, message: '',	severity: 'success'}
    }
    
    this.handlePeriodSelect=this.handlePeriodSelect.bind(this);
    this.handleValuateClick=this.handleValuateClick.bind(this);
    this.handleReturnClick=this.handleReturnClick.bind(this);
  }
  
  componentDidMount() { 
	  if(this.state.period.id == null) {
		  var url = process.env.REACT_APP_API_URL + '/valuation/period/current';
		    fetch(url)
		      .then(res => res.json())
		      .then(
		        (result) => {
		        	this.handlePeriodSelect(result, this.state.employee.id);
		        },
		        (error) => {
		        	console.log(error);
		        }
		      );
	  } else {
		  this.handlePeriodSelect(this.state.period, this.state.employee.id);
	  }
	  
  }
  
  handleValuateClick(event, valuatedEmployee) {
	var self = this;
	var valuateEmployee = [];
	valuateEmployee.push(<ValuateEmployee appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user} valuatedEmployee={valuatedEmployee} period={this.state.period}/>);
	self.props.appContext.setState({currentScreen:valuateEmployee});
  }
    
  handleReturnClick(event){
	    var self = this;
	    var personalArea = [];
	    personalArea.push(<PersonalArea appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:personalArea});
	  }  
  
  handlePeriodSelect(selectedPeriod, currentEmployeeId) {
	  this.setState({period: selectedPeriod});
	  var url = process.env.REACT_APP_API_URL + '/valuation/period/' + selectedPeriod.id + '/employee/' + currentEmployeeId + '/coemployees';
	    fetch(url)
	      .then(res => res.json())
	      .then(
	        (result) => {
	        	if(result == null) {
		        	this.setState(
		        			{coEmployees: []}
		        	);
	        	} else {
	        		this.setState(
		        			{coEmployees: result}
		        	);
	        	}
	        },
	        (error) => {
	        	console.log(error);
	        }
	      );
  }
    
  render() {
    return (
    		
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>           
    				<Grid item xs={12} >
    					<AppBar position="static">
    						<Toolbar>
    							<IconButton edge="start" color="inherit" aria-label="menu">
    								<MenuIcon />
    							</IconButton>
    							<Typography variant="h6">
    								{i18n.t("valuation360.title")}
    							</Typography>
    						</Toolbar>
    					</AppBar>
    				</Grid>
    				
    				<Grid item xs={12} justify='space-around' alignItems='stretch'>
	    				<ValuationPeriodSelect 	
		    		   		label={i18n.t("valuation.period.select.label")}  	
		    		   		value={this.state.period.id} 
		    		   		objectId={this.state.employee.id} 
		    		   		onChange={this.handlePeriodSelect} 
		    		   		helplabel={(this.state.period.startDate == null ? "" : this.state.period.startDate) + ' - ' + (this.state.period.finishDate == null ? "" : this.state.period.finishDate)}
	    				/>

	    			</Grid>
	    			
    				<Grid item xs={12}>
    					
	    					{
			    				this.state.coEmployees.map(it => (
			    						<Container  maxWidth="md">
				    					<Grid container spacing={2}>
			    						<Grid item xs={11}>
					    					<TextField
						    		            variant="outlined"
						    		            margin="normal"
						    		            required
						    		            fullWidth
						    		            id={"employeeName" + it.id}
						    		            label={i18n.t("valuation.employee.name.label")}
						    		            name={"employeeName" + it.id}
						    		            autoFocus
						    		            value={it.lastName + ' ' + it.firstName + ' ' + it.middleName} 
					    		            />
					    				</Grid>	
					    				<Grid item xs={1}>
						    				<IconButton aria-label="delete" onClick={(event => this.handleValuateClick(event, it))}>
				              					<EqualizerIcon />
				              				</IconButton>
					    				</Grid>  
					    				</Grid> 
					    				</Container>
			    				))
			    			}
		    				
	    			</Grid>  
		    		
  	    				<Grid item xs={4}>	
  	    				</Grid>	
	    				
	    				<Grid item xs={4}>
                    	  <Button
	                  		type="submit"
	                  		fullWidth
	                  		variant="contained"
	                  		color="primary"  
	                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
	                  	  >
		    					{i18n.t("return.button.label")}
	                  	  </Button>
	                  	</Grid>	
	    				<Grid item xs={4}>
    					</Grid>
    					
    			
    			</Grid>
            		
    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
			        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
			        	{this.state.snackbar.message}
			        </Alert>
			    </Snackbar>
           
      </div>
      </Container>
      
    );
  }
}

export default Valuation360;
