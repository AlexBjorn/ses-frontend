import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';

import Dictionaries from '../Dictionaries';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

import AddIcon from '@material-ui/icons/Add';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import EditIcon from '@material-ui/icons/Edit';
import ReplayIcon from '@material-ui/icons/Replay';
import ReplyIcon from '@material-ui/icons/Reply';
import SaveAltIcon from '@material-ui/icons/SaveAlt';

import Divider from '@material-ui/core/Divider';

class Projects extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				projects: [],
				message: '',
			    snackbar: {openflg: false, message: '',	severity: 'success'}
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleAdd=this.handleAdd.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		this.handleNameChange=this.handleNameChange.bind(this);
		this.handleStartDateChange=this.handleStartDateChange.bind(this);
		this.handleFinishDateChange=this.handleFinishDateChange.bind(this);
		this.handleDescriptionChange=this.handleDescriptionChange.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
	}
  
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/project/all')
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({projects: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {
			var info = {
					projects: this.state.projects
			};
			
			axios.post(process.env.REACT_APP_API_URL+'/project/change', info)
			.then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
		
	}
  
  handleReturnClick(event){
	    var self = this;
	    var dictionaries = [];
	    dictionaries.push(<Dictionaries appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:dictionaries});
  } 
  
  handleAdd(event) {
  	  var newId = -1;
  	  var prj = [];
  	  prj = this.state.projects.slice();
  	  if(prj.length > 0) {
  		  var minId = prj.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	  prj.push({
  		  id:newId,
  		  name:''
  	  });
  	  this.setState({projects:prj});
  }
  
  handleDelete(event, currentProject) {
  	  var prj = [];
  	  prj = this.state.projects.slice();
  	  var index = -1
  	  for(let i=0; i<prj.length; i++) {
  		  if(prj[i].id == currentProject.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  prj.splice(index, 1);
  	  this.setState({projects:prj});
  }
  
  handleNameChange(event, currentProject) {
	  var prj = [];
	  prj = this.state.projects.slice();
	  for(let i=0; i<prj.length; i++) {
		  if(prj[i].id == currentProject.id) {
			  prj[i].name = event.target.value;
			  this.setState({projects:prj});
			  break;
		  }
	  }
  }
  
  handleStartDateChange(date, currentProject) {
	  if(date != null) {
		  var prj = [];
		  prj = this.state.projects.slice();
		  for(let i=0; i<prj.length; i++) {
			  if(prj[i].id == currentProject.id) {
				  prj[i].startDate = date;
				  this.setState({projects:prj});
				  break;
			  }
		  }
	  }
  }
  
  handleFinishDateChange(date, currentProject) {
	  if(date != null) {
		  var prj = [];
		  prj = this.state.projects.slice();
		  for(let i=0; i<prj.length; i++) {
			  if(prj[i].id == currentProject.id) {
				  prj[i].finishDate = date;
				  this.setState({projects:prj});
				  break;
			  }
		  }
	  }
  }
  
  handleDescriptionChange(event, currentProject) {
	  alert('handleDescriptionChange');
	  var prj = [];
	  prj = this.state.projects.slice();
	  for(let i=0; i<prj.length; i++) {
		  if(prj[i].id == currentProject.id) {
			  prj[i].description = event.target.value;
			  this.setState({projects:prj});
			  break;
		  }
	  }
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
    
  render() {
	  
	  return (		  	
		
			  <Container component="main">
	    		<CssBaseline />
	    		<div>
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="fixed">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("projects.title")}
	    							</Typography>
	    							<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleAdd(event, this.props.role)}>
										<AddCircleIcon />
									</IconButton>
									<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleSaveClick(event, this.props.role)}>
										<SaveAltIcon />
									</IconButton>
									<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleReturnClick(event)}>
										<ReplyIcon />
									</IconButton>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
		    			</Grid>
		    			<Grid item xs={12}>
		    			</Grid>
	    				<Grid item xs={12}>
	    					
		    					{
				    				this.state.projects.map(it => (
				    						
				    						<Container container spacing={2}>
					    					<Grid container spacing={2}>
				    						<Grid item xs={7}>
						    					<TextField
							    		            variant="outlined"
							    		            margin="normal"
							    		            required
							    		            fullWidth
							    		            id={"projectName" + it.id}
							    		            label={i18n.t("project.name.label")}
							    		            name={"projectName" + it.id}
							    		            autoFocus
							    		            value={it.name}
							    		            onChange={(event => this.handleNameChange(event, it))}
						    		            />
						    				</Grid>	
						    				<Grid item xs={4}>
							        			<TextField
								    		        id={"projectStartDatePicker" + it.id}
								    		        label={i18n.t("project.startDate.label")}
								    		        type="date"
								    		        
								    		        InputLabelProps={{
								    		          shrink: true,
								    		        }}
							    					value={it.startDate}
							    					onChange={(event) => this.handleStartDateChange(event, it)}
							    		        />
					        			   
							        			<TextField
								    		        id={"projectFinishDatePicker" + it.id}
								    		        label={i18n.t("project.finishDate.label")}
								    		        type="date"
								    		        
								    		        InputLabelProps={{
								    		          shrink: true,
								    		        }}
							    					value={it.finishDate}
							    					onChange={(event) => this.handleEndDateChange(event, it)}
							    		        />
					        			   </Grid>
						    				<Grid item xs={1}>
						    					<IconButton 
			    									aria-label="delete" 
			    						  			onClick={(event => this.handleDelete(event, it))}
			    				  				>
			    									<DeleteIcon />
			    								</IconButton>
						    				</Grid>  
						    				<Grid item xs={12} justify='space-around' alignItems='stretch'>
							              		 <TextField
								                     id={"description"+it.id}
								                     label={i18n.t("project.description.placeholder")}
								                     multiline
								                     fullWidth
								                     rows={2}
								                     variant="outlined"
								                     value={it.description}	 
							              		 	 onChange={(event => this.handleDescriptionChange(event, it))}	 
							                     />
							              	</Grid>
							              	<Grid item xs={12}>
							              		<Divider/>
							              	</Grid>
						    				</Grid> 
						    				</Container>
						    				
				    				))
				    			}
			    				
		    			</Grid>  
			    		
	    			</Grid>
	            		
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container>
				
    );
  }
}

export default Projects;
