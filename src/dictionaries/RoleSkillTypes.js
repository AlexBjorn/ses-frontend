import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import Dictionaries from '../Dictionaries';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';
import RoleSelect from '../select/RoleSelect';
import SkillTypeSelect from '../select/SkillTypeSelect';

import ReplayIcon from '@material-ui/icons/Replay';
import ReplyIcon from '@material-ui/icons/Reply';
import SaveAltIcon from '@material-ui/icons/SaveAlt';

import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';

import { makeStyles, withStyles } from '@material-ui/core/styles';

import StyledTreeItem from '../StyledTreeItem';

const useStyles = makeStyles({
	  root: {
	    height: 110,
	    flexGrow: 1,
	    maxWidth: 400,
	  },
	});

class RoleSkillTypes extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				role: null,
				skills: [],
				message: '',
			    snackbar: {openflg: false, message: '',	severity: 'success'},
			    changedRoleSkills: []
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleAdd=this.handleAdd.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		this.handleRoleSelect=this.handleRoleSelect.bind(this);
		this.handleSkillTypeSelect=this.handleSkillTypeSelect.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
		this.updateSkillTree=this.updateSkillTree.bind(this);
		this.handleCheckboxChange=this.handleCheckboxChange.bind(this);
	}
  
	componentDidMount() {
		
	}
	
	updateSkillTree() {
		var url = process.env.REACT_APP_API_URL+'/roleskill/role/'+this.state.role.id;
		fetch(url)
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({skills: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {
			var info = {
					roleSkillTypes: this.state.roleskilltypes
			};
			
			axios.post(process.env.REACT_APP_API_URL+'/roleskilltype/change', info)
			.then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
		
	}
  
  handleReturnClick(event){
	    var self = this;
	    var dictionaries = [];
	    dictionaries.push(<Dictionaries appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:dictionaries});
  } 
  
  handleAdd(event) {
  	  var newId = -1;
  	  var rst = [];
  	  rst = this.state.roleskilltypes.slice();
  	  if(rst.length > 0) {
  		  var minId = rst.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	  rst.push({
  		  id: newId,
  		  role: {id: null, name: ''},
  		  skillType: {id: null, name: '', parentId: 0}
  	  });
  	  this.setState({roleskilltypes:rst});
  }
  
  handleDelete(event, current) {
  	  var rst = [];
  	  rst = this.state.roleskilltypes.slice();
  	  var index = -1
  	  for(let i=0; i<rst.length; i++) {
  		  if(rst[i].id == current.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  rst.splice(index, 1);
  	  this.setState({roleskilltypes:rst});
  }
  
  handleRoleSelect(selectedRole, roleSkillId) {
	  this.setState({role: selectedRole});
	  this.updateSkillTree();
  }
  
  handleSkillTypeSelect(skillType, roleSkillTypeId) {
	  var rst = [];
	  rst = this.state.roleskilltypes.slice();
	  for(let i=0; i<rst.length; i++) {
		  if(rst[i].id == roleSkillTypeId) {
			  rst[i].skillType = skillType;
			  this.setState({roleskilltypes:rst});
			  break;
		  }
	  }
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
  
  showTree(item) {
	  return (
		<StyledTreeItem key={item.skill.id} nodeId={item.skill.id} labelText={item.skill.name} onChangeCheckbox={this.handleCheckboxChange}>
			{Array.isArray(item.children) ? item.children.map((node) => this.showTree(node)) : null}
		</StyledTreeItem>
	  )
  }
  
  handleCheckboxChange(checked, nodeId) {
	  var rolesks = this.state.chengedRoleSkills.slice();
	  for(let i=0; i<rolesks.length; i++) {
		  if(rolesks[i].id == nodeId) {
			  rolesks[i].skill.checked=checked;
			  this.setState({skills:rolesks});
			  break;
		  }
	  }
  }
    
  render() {
	  const classes = useStyles;
	  return (		  	
		
			  <Container component="main">
	    		<CssBaseline />
	    		<div align="left">
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="fixed">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("roleskilltypes.title")}
	    							</Typography>
	    							<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleSaveClick(event, this.props.role)}>
										<SaveAltIcon />
									</IconButton>
									<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleReturnClick(event)}>
										<ReplyIcon />
									</IconButton>
									<RoleSelect 	
										label={i18n.t("roleskilltypes.role.select.label")} 		
										value={this.state.role==null?null:this.state.role.id} 	
										objectId={this.state.role==null?null:this.state.role.id} 
										onChange={this.handleRoleSelect} 
									/>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
		    				<TreeView
			    				className={classes.root}
			    		      		defaultCollapseIcon={<ExpandMoreIcon />}
			    		      		defaultExpandIcon={<ChevronRightIcon />}
			    				onNodeSelect={this.handleNodeSelect}
			    			>
		    					{
		    						this.state.skills.map(
		    								it => (
		    										this.showTree(it)
		    								)
		    						)
		    					}
		    				</TreeView>	
	    				
			    				
		    			</Grid>  	
	    			
	    			</Grid>
	            		
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container>  
			  
							
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(RoleSkillTypes);
