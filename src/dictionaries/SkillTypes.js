import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import Dictionaries from '../Dictionaries';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

class SkillTypes extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				types: [],
			    snackbar: {openflg: false, message: '',	severity: 'success'}
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleAdd=this.handleAdd.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		this.handleNameChange=this.handleNameChange.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
	}
  
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/skill/type/all')
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({types: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {
			var info = {
					skillTypes: this.state.types
			};
			
			axios.post(process.env.REACT_APP_API_URL+'/skill/type/change', info)
			.then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
	}
  
  handleReturnClick(event){
	    var self = this;
	    var dictionaries = [];
	    dictionaries.push(<Dictionaries appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:dictionaries});
  } 
  
  handleAdd(event) {
  	  var newId = -1;
  	  var tps = [];
  	  tps = this.state.types.slice();
  	  if(tps.length > 0) {
  		  var minId = tps.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	  
  	  tps.push({
  		  id:newId,
  		  name:''
  	  });
  	  this.setState({types:tps});
  }
  
  handleDelete(event, currentSkillType) {
  	  var tps = [];
  	  tps = this.state.types.slice();
  	  var index = -1
  	  for(let i=0; i<tps.length; i++) {
  		  if(tps[i].id == currentSkillType.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  tps.splice(index, 1);
  	  this.setState({types:tps});
  }
  
  handleNameChange(event, currentSkillType) {
	  var tps = [];
	  tps = this.state.types.slice();
	  for(let i=0; i<tps.length; i++) {
		  if(tps[i].id == currentSkillType.id) {
			  tps[i].name = event.target.value;
			  this.setState({types:tps});
			  break;
		  }
	  }
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
    
  render() {
	  return (		  	
			  <Container component="main">
	    		<CssBaseline />
	    		<div>
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="static">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("skill.types.title")}
	    							</Typography>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={6}>
	    				</Grid>
	    				<Grid item xs={3}>
	    					<Button
			            		type="submit"
			            		fullWidth
			            		variant="contained"
			            		color="primary"  
			            		onClick={(event) => this.handleAdd(event, this.props.role)}
			            	>
	    						{i18n.t("add.skill.type.button.label")}
	    					</Button>
		    			</Grid>
		    			<Grid item xs={3}>
	    				</Grid>
	    				<Grid item xs={12}>
	    					
		    					{
				    				this.state.types.map(it => (
				    						<Container  maxWidth="xs">
					    					<Grid container spacing={2}>
				    						<Grid item xs={11}>
						    					<TextField
							    		            variant="outlined"
							    		            margin="normal"
							    		            required
							    		            fullWidth
							    		            id={"skillTypeName" + it.id}
							    		            label={i18n.t("skill.type.name.label")}
							    		            name={"skillTypeName" + it.id}
							    		            autoFocus
							    		            value={it.name}
							    		            onChange={(event => this.handleNameChange(event, it))}
						    		            />
						    				</Grid>	
						    				<Grid item xs={1}>
						    					<IconButton 
			    									aria-label="delete" 
			    						  			onClick={(event => this.handleDelete(event, it))}
			    				  				>
			    									<DeleteIcon />
			    								</IconButton>
						    				</Grid>  
						    				</Grid> 
						    				</Container>
				    				))
				    			}
			    				
		    			</Grid>  
			    		
	  	    				<Grid item xs={3}>	
	  	    				</Grid>	
		    				<Grid item xs={3}>
	  	    				  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleSaveClick(event, this.props.role)}
		                  	  >
	  	    				  	{i18n.t("save.button.label")}
		                  	  </Button>	
		                  	</Grid>	
		    				<Grid item xs={3}>
	                    	  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
		                  	  >
			    					{i18n.t("return.button.label")}
		                  	  </Button>
		                  	</Grid>	
		    				<Grid item xs={3}>
	    					</Grid>
	    					
	    			
	    			</Grid>
	            		
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container>  
			  
    );
  }
}

export default SkillTypes;
