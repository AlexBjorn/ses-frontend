import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';
import Box from '@material-ui/core/Box';
import Dictionaries from '../Dictionaries';
import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';
import SkillTypeSelect from '../select/SkillTypeSelect';
import ButtonGroup from '@material-ui/core/ButtonGroup';

import TreeView from '@material-ui/lab/TreeView';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import TreeItem from '@material-ui/lab/TreeItem';
import AddIcon from '@material-ui/icons/Add';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import EditIcon from '@material-ui/icons/Edit';
import ReplayIcon from '@material-ui/icons/Replay';
import ReplyIcon from '@material-ui/icons/Reply';

import { makeStyles, withStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
	  root: {
	    height: 110,
	    flexGrow: 1,
	    maxWidth: 400,
	  },
	});

class Skills extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				skills: [],
			    snackbar: {openflg: false, message: '',	severity: 'success'},
			    dialog: {open: false},
			    selectedNode: null,
			    editedNode: null
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleAdd=this.handleAdd.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		this.handleNameChange=this.handleNameChange.bind(this);
		this.handleSkillTypeSelect=this.handleSkillTypeSelect.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);
		this.showTree=this.showTree.bind(this);
		this.handleDialogClose=this.handleDialogClose.bind(this);
		this.handleNodeSelect=this.handleNodeSelect.bind(this);
		this.updateTree=this.updateTree.bind(this);
		this.handleEdit=this.handleEdit.bind(this);
	}
  
	componentDidMount() {
		this.updateTree();
	}
	
	updateTree() {
		var url = process.env.REACT_APP_API_URL+'/skill/all';
		fetch(url)
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({skills: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {			
		axios.post(process.env.REACT_APP_API_URL+'/skill/save', this.state.editedNode)
			.then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
					this.updateTree();
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
		this.handleDialogClose(event);
	}
  
  handleReturnClick(event){
	    var self = this;
	    var dictionaries = [];
	    dictionaries.push(<Dictionaries appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:dictionaries});
  } 
  
  handleDialogClose(event) {
	  this.setState({dialog: {open: false}});
  }
  
  handleAdd(event) {
	  let pId=0;
	  if(this.state.selectedNode != null) {
		  pId=this.state.selectedNode.id;
	  }
	  this.setState({editedNode: {id: -1, name: '', parentId: pId}});
	  this.setState({dialog: {open: true}});
  }
  
  handleEdit(event) {
	  this.setState({editedNode: this.state.selectedNode});
	  this.setState({dialog: {open: true}});
  }
  
  handleDelete(event) {
	  axios.post(process.env.REACT_APP_API_URL+'/skill/delete/'+this.state.selectedNode.id)
		.then((response) => {
			console.log(response);
			if(response.status == 200){
				this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.success.message"), 
					severity: "success"
				}});
				this.updateTree();
			} else {
				this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.error.message"), 
					severity: "error"}});
			}
	   })
	   .catch((error) => {
		     console.log(error);
		     this.setState({snackbar:{
					openflg: true, 
					message: i18n.t("save.error.message"), 
					severity: "error"}});
	});
  }
  
  handleNameChange(event) {
	  let skill = this.state.editedNode;
	  skill.name = event.target.value;
	  this.setState({editedNode: skill});
  }
  
  handleSkillTypeSelect(newSkillType, skillId) {
	  var skls = [];
	  skls = this.state.skills.slice();
	  for(let i=0; i<skls.length; i++) {
		  if(skls[i].id == skillId) {
			  skls[i].type = newSkillType;
			  this.setState({skills:skls});
			  break;
		  }
	  }
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
  
  showTree(item) {
	  
	  return (
			 
			  			<TreeItem key={item.skill.id} nodeId={item.skill.id} label={item.skill.name}>
			  				{Array.isArray(item.children) ? item.children.map((node) => this.showTree(node)) : null}
			  			</TreeItem>
			  		
	  )
  }
  
  handleNodeSelect(event, value) {
	  var url = process.env.REACT_APP_API_URL+'/skill/'+value;
		fetch(url)
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({selectedNode: result});
					},
					(error) => {
						console.log(error);
					}
			);
  } 
    
  render() {
	  const classes = useStyles;
	  
	  return (
			  <div align="left">
			  <Container component="main">
	    		<CssBaseline />
	    		
	    		<div>
	    			<Grid container spacing={2} direction="column"
	    				  justify="flex-start"
	    					  alignItems="flex-start">           
	    				<Grid item xs={12}>
	    					<AppBar position="fixed">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("skills.title")}
	    							</Typography>
	    							<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleAdd(event, this.props.role)}>
    									<AddCircleIcon />
    								</IconButton>
    								<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleEdit(event, this.props.role)}>
										<EditIcon />
									</IconButton>
									<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleDelete(event, this.props.role)}>
										<DeleteIcon />
									</IconButton>
									<IconButton color="inherit" aria-label="menu" onClick={(event) => this.handleReturnClick(event)}>
										<ReplyIcon />
									</IconButton>
									
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
		    			</Grid>
		    			<Grid item xs={12}>
	    				</Grid>
	    				<Grid item xs={12}>
	    				<TreeView
	    				className={classes.root}
	    		      		defaultCollapseIcon={<ExpandMoreIcon />}
	    		      		defaultExpandIcon={<ChevronRightIcon />}
	    				onNodeSelect={this.handleNodeSelect}
	    			>
	    					{
	    						this.state.skills.map(
	    								it => (
	    										this.showTree(it)
	    								)
	    						)
	    					}
	    				</TreeView>
	    				</Grid>
	    					    			
	    			</Grid>
	    			
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container> 
	  	
	      <Dialog open={this.state.dialog.open} onClose={this.handleDialogClose} aria-labelledby="form-dialog-title">
	        <DialogTitle id="form-dialog-title">Редактирование</DialogTitle>
	        <DialogContent>
	          <DialogContentText>
	            {this.state.selectedNode==null?'':this.state.selectedNode.name} 
	          </DialogContentText>
	          <TextField
	            autoFocus
	            margin="dense"
	            id="name"
	            label="Наименование компетенции"
	            defaultValue={this.state.selectedNode==null?'':this.state.selectedNode.name}
	            fullWidth
	            onChange={(event => this.handleNameChange(event))}
	          />
	        </DialogContent>
	        <DialogActions>
	          <Button onClick={(event) => this.handleSaveClick(event, this.props.role)} color="primary">
	            Сохранить
	          </Button>
	          <Button onClick={this.handleDialogClose} color="primary">
	            Отмена
	          </Button>
	        </DialogActions>
	      </Dialog>  
	      </div>
    );
  }
}

export default withStyles(useStyles, { withTheme: true })(Skills);
