import React, { Component } from 'react';

import axios from 'axios';
import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';

import Dictionaries from '../Dictionaries';

import Snackbar from '@material-ui/core/Snackbar';
import Alert from '@material-ui/lab/Alert';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

class ValuationPeriods extends Component {
  
	constructor(props) {
		super(props);
		this.state={
				valuationPeriods: [],
			    snackbar: {openflg: false, message: '',	severity: 'success'}
		}
		    
		this.handleSaveClick=this.handleSaveClick.bind(this);
		this.handleReturnClick=this.handleReturnClick.bind(this);
		this.handleAdd=this.handleAdd.bind(this);
		this.handleDelete=this.handleDelete.bind(this);
		
		this.handleNameChange=this.handleNameChange.bind(this);
		this.handleStartDateChange=this.handleStartDateChange.bind(this);
		this.handleFinishDateChange=this.handleFinishDateChange.bind(this);
		this.handleSnackBarClose=this.handleSnackBarClose.bind(this);		
	}
  
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/valuation/period/all')
			.then(res => res.json())
			.then(
					(result) => {
						this.setState({valuationPeriods: result});
					},
					(error) => {
						console.log(error);
					}
			);
	}
  
	handleSaveClick(event) {
			var info = {
					valuationPeriods: this.state.valuationPeriods
			};
			
			axios.post(process.env.REACT_APP_API_URL+'/valuation/period/change', info)
			.then((response) => {
				console.log(response);
				if(response.status == 200){
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.success.message"), 
						severity: "success"
					}});
				} else {
					this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
				}
		   })
		   .catch((error) => {
			     console.log(error);
			     this.setState({snackbar:{
						openflg: true, 
						message: i18n.t("save.error.message"), 
						severity: "error"}});
		});
		
	}
  
  handleReturnClick(event){
	    var self = this;
	    var dictionaries = [];
	    dictionaries.push(<Dictionaries appContext={self.props.appContext} role={self.state.loginRole} user={self.props.user}/>);
	    self.props.appContext.setState({currentScreen:dictionaries});
  } 
  
  handleAdd(event) {
  	  var newId = -1;
  	  var vprd = [];
  	  vprd = this.state.valuationPeriods.slice();
  	  if(vprd.length > 0) {
  		  var minId = vprd.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	  vprd.push({
  		  id:newId,
  		  name:''
  	  });
  	  this.setState({valuationPeriods:vprd});
  }
  
  handleDelete(event, current) {
  	  var vprd = [];
  	  vprd = this.state.valuationPeriods.slice();
  	  var index = -1
  	  for(let i=0; i<vprd.length; i++) {
  		  if(vprd[i].id == current.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  vprd.splice(index, 1);
  	  this.setState({valuationPeriods:vprd});
  }
  

  handleNameChange(event, current) {
	  var vprd = [];
	  vprd = this.state.valuationPeriods.slice();
	  for(let i=0; i<vprd.length; i++) {
		  if(vprd[i].id == current.id) {
			  vprd[i].name = event.target.value;
			  this.setState({valuationPeriods:vprd});
			  break;
		  }
	  }
  }
  
  handleStartDateChange(date, current) {
	  if(date != null) {
		  var vprd = [];
		  vprd = this.state.valuationPeriods.slice();
		  for(let i=0; i<vprd.length; i++) {
			  if(vprd[i].id == current.id) {
				  vprd[i].startDate = date;
				  this.setState({valuationPeriods:vprd});
				  break;
			  }
		  }
	  }
  }
  
  handleFinishDateChange(date, current) {
	  if(date != null) {
		  var vprd = [];
		  vprd = this.state.valuationPeriods.slice();
		  for(let i=0; i<vprd.length; i++) {
			  if(vprd[i].id == current.id) {
				  vprd[i].finishDate = date;
				  this.setState({valuationPeriods:vprd});
				  break;
			  }
		  }
	  }
  }
  
  handleSnackBarClose() {
	  this.setState({snackbar:{openflg: false, message: '', severity: "success"}});
  }
  
  render() {
	  
	  return (		  	
			  <Container component="main">
	    		<CssBaseline />
	    		<div>
	    			<Grid container spacing={2}>           
	    				<Grid item xs={12}>
	    					<AppBar position="static">
	    						<Toolbar>
	    							<IconButton edge="start" color="inherit" aria-label="menu">
	    								<MenuIcon />
	    							</IconButton>
	    							<Typography variant="h6">
	    								{i18n.t("valuation.period.title")}
	    							</Typography>
	    						</Toolbar>
	    					</AppBar>
	    				</Grid>
	    				<Grid item xs={6}>
	    				</Grid>
	    				<Grid item xs={3}>
	    					<Button
			            		type="submit"
			            		fullWidth
			            		variant="contained"
			            		color="primary"  
			            		onClick={(event) => this.handleAdd(event, this.props.role)}
			            	>
	    						{i18n.t("add.valuation.period.button.label")}
	    					</Button>
		    			</Grid>
		    			<Grid item xs={3}>
	    				</Grid>
	    				<Grid item xs={12}>
	    					
		    					{
				    				this.state.valuationPeriods.map(it => (
				    						<Container  maxWidth="md">
					    					<Grid container spacing={2}>
				    						<Grid item xs={11}>
						    					<TextField
							    		            variant="outlined"
							    		            margin="normal"
							    		            required
							    		            fullWidth
							    		            id={"periodName" + it.id}
							    		            label={i18n.t("valuation.period.name.label")}
							    		            name={"periodName" + it.id}
							    		            autoFocus
							    		            value={it.name}
							    		            onChange={(event => this.handleNameChange(event, it))}
						    		            />
						    				
						    					<TextField
								    		        id={"projectStartDatePicker" + it.id}
								    		        label={i18n.t("valuation.period.startDate.label")}
								    		        type="date"
								    		        
								    		        InputLabelProps={{
								    		          shrink: true,
								    		        }}
							    					value={it.startDate}
							    					onChange={(event) => this.handleStartDateChange(event, it)}
							    		        />
					        			   
							        			<TextField
								    		        id={"projectFinishDatePicker" + it.id}
								    		        label={i18n.t("valuation.period.finishDate.label")}
								    		        type="date"
								    		        
								    		        InputLabelProps={{
								    		          shrink: true,
								    		        }}
							    					value={it.finishDate}
							    					onChange={(event) => this.handleFinishDateChange(event, it)}
							    		        />
						    				</Grid>	
						    				<Grid item xs={1}>
						    					<IconButton 
			    									aria-label="delete" 
			    						  			onClick={(event => this.handleDelete(event, it))}
			    				  				>
			    									<DeleteIcon />
			    								</IconButton>
						    				</Grid>  
						    				</Grid> 
						    				</Container>
				    				))
				    			}
			    				
		    			</Grid>  
			    		
	  	    				<Grid item xs={3}>	
	  	    				</Grid>	
		    				<Grid item xs={3}>
	  	    				  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleSaveClick(event, this.props.role)}
		                  	  >
	  	    				  	{i18n.t("save.button.label")}
		                  	  </Button>	
		                  	</Grid>	
		    				<Grid item xs={3}>
	                    	  <Button
		                  		type="submit"
		                  		fullWidth
		                  		variant="contained"
		                  		color="primary"  
		                  		onClick={(event) => this.handleReturnClick(event, this.props.role)}
		                  	  >
			    					{i18n.t("return.button.label")}
		                  	  </Button>
		                  	</Grid>	
		    				<Grid item xs={3}>
	    					</Grid>
	    					
	    			
	    			</Grid>
	            		
	    			<Snackbar open={this.state.snackbar.openflg} autoHideDuration={3000} onClose={this.handleSnackBarClose} anchorOrigin={{ vertical: 'top', horizontal: 'right' }}>
				        <Alert onClose={this.handleSnackBarClose} severity={this.state.snackbar.severity} variant="filled">
				        	{this.state.snackbar.message}
				        </Alert>
				    </Snackbar>
	           
	      </div>
	      </Container>  
			  
    );
  }
}

export default ValuationPeriods;
