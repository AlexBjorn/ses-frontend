import React, { Component } from 'react';
import axios, {post} from 'axios';

import DateFnsUtils from '@date-io/date-fns';
import { MuiPickersUtilsProvider, KeyboardDatePicker } from '@material-ui/pickers';
import i18n from 'i18next';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import Dropzone from 'react-dropzone';


class Employee extends Component {
	
  constructor(props) {
    super(props);
    
    this.state={
    		employee: props.employee,
    		foto: null
    };
    
    this.handleLastNameChange=this.handleLastNameChange.bind(this);
    this.handleFirstNameChange=this.handleFirstNameChange.bind(this);
    this.handleMiddleNameChange=this.handleMiddleNameChange.bind(this);
    this.handleEmailChange=this.handleEmailChange.bind(this);
    this.handleBirthDateChange=this.handleBirthDateChange.bind(this);
    this.onDrop=this.onDrop.bind(this);
    this.showFoto=this.showFoto.bind(this);
  }
  
  componentDidMount() {
		var url = process.env.REACT_APP_API_URL + '/employee/' + this.state.employee.id + '/download/foto';
		fetch(url)
		.then(response => {
			  const headers = response.headers;
		      const filename =  response.headers.get('Content-Disposition').split('filename=')[1];
		      response.blob().then(blob => {
		    	  this.setState({foto: new File([blob], 'untitled', { type: blob.type })});
		    	  console.log("file: " + this.state.foto);
//		        let url = window.URL.createObjectURL(blob);
//		        let a = document.createElement('a');
//		        a.href = url;
//		        a.download = filename;
//		        a.click();
		    });
		 });
  }
      
  handleLastNameChange(newValue) {
	  var changedEmployee=this.state.employee;
	  changedEmployee.lastName=newValue;
	  this.props.onChange(changedEmployee);
  }
  
  handleFirstNameChange(newValue) {
	  var changedEmployee=this.state.employee;
	  changedEmployee.firstName=newValue;
	  this.props.onChange(changedEmployee);
  }
  
  handleMiddleNameChange(newValue) {
	  var changedEmployee=this.state.employee;
	  changedEmployee.middleName=newValue;
	  this.props.onChange(changedEmployee);
  }
  
  handleEmailChange(newValue) {
	  var changedEmployee=this.state.employee;
	  changedEmployee.email=newValue;
	  this.props.onChange(changedEmployee);
  }
  
  handleBirthDateChange(newValue) {
	  var changedEmployee=this.state.employee;
	  changedEmployee.birthday=newValue;
	  this.props.onChange(changedEmployee);
  }
  
  	  onDrop(file){
  		  console.log("file: " + file);
  		  this.setState({foto: file});
	    const url = process.env.REACT_APP_API_URL+'/employee/'+this.state.employee.id+'/upload/foto';
	    console.log(url);
	    console.log(this.state.foto);
	    const config = {
	        headers: {
	            'Content-Type': 'multipart/form-data'
	        }
	    };
	    const formData = new FormData();
	    formData.append('file', file, file.name);
	    console.log(formData);
	    axios.post(url,
    		  formData
    		).then((result) => {
    		  console.log(result);
    		})
    		.catch((error) => {
    		  console.log(error);
    		});
	  }

  	onFileDrop = acceptedFiles =>{ 
  	  		
  	  		// Update the state 
  	  		this.onDrop(acceptedFiles[0]); 
  	  		
  	  		};
  	  		
  	showFoto() {
  		if(this.state.foto != null) {
  			return <div><img src={this.state.foto.preview} height="200px" width="180px"/></div>
  		} 
  	}  		
  	  		
  render() {
    return (
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}> 
    				<Grid item xs={3}>
    					<Grid container spacing={2}>
    						<Grid item xs={12}>		
    							<Dropzone onDrop={this.onFileDrop} accept="image/*">
			                    	{this.showFoto}
			                    </Dropzone>
			                </Grid>
			                <Grid item xs={12}>		
			    				<TextField
				    		        id="date"
				    		        label={i18n.t("resume.birthdate")}
				    		        type="date"
				    		        
				    		        InputLabelProps={{
				    		          shrink: true,
				    		        }}
			    					value={this.state.employee.birthday}
			    					onChange={(event) => this.handleBirthDateChange(event.target.value)}
			    		        />
			    			</Grid>
			            </Grid>
			        </Grid>
    				<Grid item xs={9}>
						<Grid container spacing={2}>
							<Grid item xs={12}>	
			    				<TextField
						            variant="outlined"
						            margin="normal"
						            required
						            fullWidth
						            id="lastname"
						            label={i18n.t("resume.lastname.label")}
						            name="lastname"
						            autoFocus
						            value={this.state.employee.lastName}
						            onChange={(event) => this.handleLastNameChange(event.target.value)}
			    				/>
			    			</Grid>
			    			
			    			<Grid item xs={12}>
			    				<TextField
						            variant="outlined"
						            margin="normal"
						            required
						            fullWidth
						            id="firstname"
						            label={i18n.t("resume.firstname.label")}
						            name="firstname"
						            autoFocus
						            value={this.state.employee.firstName}
						            onChange={(event) => this.handleFirstNameChange(event.target.value)}
			    				/>
			    			</Grid>
			    			<Grid item xs={12}>
			    				<TextField
						            variant="outlined"
						            margin="normal"
						            required
						            fullWidth
						            id="middlename"
						            label={i18n.t("resume.middlename.label")}
						            name="middlename"
						            autoFocus
						            value={this.state.employee.middleName}
						            onChange={(event) => this.handleFirstNameChange(event.target.value)}
			    				/>
			    			</Grid>
			    			<Grid item xs={12}>		
			    				<TextField
				                    variant="outlined"
				                    margin="normal"
				                    required
				                    fullWidth
				                    id="email"
				                    label={i18n.t("resume.email.label")}
				                    name="email"
				                    autoComplete="email"
				                    value={this.state.employee.email}
			    					onChange={(event) => this.handleEmailChange(event.target.value)}
			                   />
			    			</Grid>
			    		</Grid>
			    	</Grid>	    				
	    		</Grid>
	    	</div>
	    </Container>
           
           
      
    );
  }
}

export default Employee;
