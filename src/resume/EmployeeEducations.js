import React, { Component } from 'react';

import Box from '@material-ui/core/Box';
import i18n from 'i18next';
import EducationLevelSelect from '../select/EducationLevelSelect';

import DeleteIcon from '@material-ui/icons/Delete';

import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import TextField from '@material-ui/core/TextField';

class EmployeeEducations extends Component {
	
  constructor(props){
    super(props);
    this.state={
      educations:[]
    }
    
    this.handleAdd=this.handleAdd.bind(this);
    this.handleDelete=this.handleDelete.bind(this);
    
    this.handleEducationLevelSelect=this.handleEducationLevelSelect.bind(this);
    this.handleGraduatedDateChange=this.handleGraduatedDateChange.bind(this);
    this.handleInstitutionChange=this.handleInstitutionChange.bind(this);
    this.handleFacultyChange=this.handleFacultyChange.bind(this);
    this.handleSpecializationChange=this.handleSpecializationChange.bind(this);    
  }
  
  componentDidMount() {
	  var url = process.env.REACT_APP_API_URL+'/employee/education/'+this.props.employee.id;
	  fetch(url)
	      .then(res => res.json())
	      .then(
	        (result) => {
	        	this.setState({educations: result});
	        },
	        (error) => {
	        	console.log(error);
	        }
	      );
  }  
  
  handleAdd(event) {
  	  var newId = -1;
  	  var empedu = [];
  	  empedu = this.state.educations.slice();
  	  if(empedu.length > 0) {
  		  var minId = empedu.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	empedu.push({
  		  id:newId,
  		  employee:this.props.employee,
  		  educationLevel:{id:null,name:''},
  		  institution:'',
  		  faculty:'',
  		  specialization:'',
  		  graduatedDate:null
  	  });
  	  this.setState({educations:empedu});
  	  this.props.onChange(empedu);
    }
  
  	handleDelete(event, currentEmployeeEducation) {
  	  var empedu = [];
  	  empedu = this.state.educations.slice();
  	  var index = -1
  	  for(let i=0; i<empedu.length; i++) {
  		  if(empedu[i].id == currentEmployeeEducation.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  empedu.splice(index, 1);
  	  this.setState({educations:empedu});
  	  this.props.onChange(empedu);
    }
  	
  	handleEducationLevelSelect(newLevel, educationId) {
	  var empedu = [];
	  empedu = this.state.educations.slice();
	  for(let i=0; i<empedu.length; i++) {
		  if(empedu[i].id == educationId) {
			  empedu[i].educationLevel = newLevel;
			  this.setState({educations:empedu});
			  this.props.onChange(empedu);
			  break;
		  }
	  }
    }
  	
  	handleGraduatedDateChange(date, employeeEducation) {
  		if(date != null) {
		  	  var empedu = [];
		  	  empedu = this.state.educations.slice();
		  	  for(let i=0; i<empedu.length; i++) {
		  		  if(empedu[i].id == employeeEducation.id) {
		  			  empedu[i].graduatedDate = date;
		  			  this.setState({educations:empedu});
		  			  this.props.onChange(empedu);
		  			  break;
		  		  }
		  	  }
  		}
    }
  	
  	
  	handleInstitutionChange(event, employeeEducation) {
    	  var empedu = [];
    	  empedu = this.state.educations.slice();
    	  for(let i=0; i<empedu.length; i++) {
    		  if(empedu[i].id == employeeEducation.id) {
    			  empedu[i].institution = event.target.value;
    			  this.setState({educations:empedu});
    			  this.props.onChange(empedu);
    			  break;
    		  }
    	  }
      }
  	
  	handleFacultyChange(event, employeeEducation) {
  	  var empedu = [];
  	  empedu = this.state.educations.slice();
  	  for(let i=0; i<empedu.length; i++) {
  		  if(empedu[i].id == employeeEducation.id) {
  			  empedu[i].faculty = event.target.value;
  			  this.setState({educations:empedu});
  			  this.props.onChange(empedu);
  			  break;
  		  }
  	  }
    }
  	
  	handleSpecializationChange(event, employeeEducation) {
  	  var empedu = [];
  	  empedu = this.state.educations.slice();
  	  for(let i=0; i<empedu.length; i++) {
  		  if(empedu[i].id == employeeEducation.id) {
  			  empedu[i].specialization = event.target.value;
  			  this.setState({educations:empedu});
  			  this.props.onChange(empedu);
  			  break;
  		  }
  	  }
    }
    
  render() {
    return (
    		
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>  
    				<Grid item xs={9}>
    				</Grid>
    				<Grid item xs={3}>
    					<Button
		            		type="submit"
		            		fullWidth
		            		variant="contained"
		            		color="primary"  
		            		onClick={(event) => this.handleAdd(event, this.props.role)}
		            	>
    						{i18n.t("resume.addeducation.button.label")}
    					</Button>
	    			</Grid>
	    		
	    			<Grid item xs={12}>
	    		{
	    			this.state.educations.map(item => (
        			   <Box>
	        			   <Grid container spacing={2}>
		        			   <Grid item xs={3}>
			        			   <EducationLevelSelect 
			        			   		label={i18n.t("resume.education.level.select.label")} 
			        			   		value={item.educationLevel.id} objectId={item.id} 
			        			   		onChange={this.handleEducationLevelSelect} 
			        			   />
		        			   </Grid>
				        		<Grid item xs={3}>
				        			<TextField
					    		        id={"graduatedDatePicker"+item.id}
					    		        label={i18n.t("resume.education.graduatedDate.label")}
					    		        type="date"
					    		        variant="outlined"
					    		        InputLabelProps={{
					    		          shrink: true,
					    		        }}
				    					value={item.graduatedDate}
				    					onChange={(event) => this.handleGraduatedDateChange(event, item)}
				    		        />
		        			   </Grid>	
				        							        			
		              		   <Grid item xs={5}>
				              		 
			              		   <TextField
								            variant="outlined"
								            margin="normal"
								            required
								            fullWidth
								            id={"institution" + item.id}
								            label={i18n.t("resume.education.institution.label")}
								            name={"institution" + item.id}
								            autoFocus
								            value={item.institution}
								            onChange={(event) => this.handleInstitutionChange(event, item)}
				    				/>
		              		   </Grid>
			              		   
			              	   <Grid item xs={1}>
			              			<IconButton aria-label="delete" onClick={(event => this.handleDelete(event, item))}>
			              				<DeleteIcon />
			              			</IconButton>
	              			   </Grid>
				              	
				              	<Grid item xs={6}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"faculty" + item.id}
							            label={i18n.t("resume.education.faculty.label")}
							            name={"faculty" + item.id}
							            autoFocus
							            value={item.faculty}
							            onChange={(event) => this.handleFacultyChange(event, item)}
				    				/>
		              		   </Grid>		 
				              	
				              	<Grid item xs={6}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            fullWidth
							            id={"specialization" + item.id}
							            label={i18n.t("resume.education.specialization.label")}
							            name={"specialization" + item.id}
							            autoFocus
							            value={item.specialization}
							            onChange={(event) => this.handleSpecializationChange(event, item)}
				    				/>
		              		   </Grid>		 

				              
	        			   </Grid>
	        			 </Box>
        			   
        	   ))}
           </Grid>
           </Grid>
           </div>
      </Container>
    		
     );
  }
}

export default EmployeeEducations;
