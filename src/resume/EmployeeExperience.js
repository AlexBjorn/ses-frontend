import React, { Component } from 'react';
import Box from '@material-ui/core/Box';

import i18n from 'i18next';

import DeleteIcon from '@material-ui/icons/Delete';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

const style = {
  margin: 15,
};

class EmployeeExperience extends Component {
	
  constructor(props){
    super(props);
    this.state={
      experiences:[]
    }
    
    this.handleAdd=this.handleAdd.bind(this);
    this.handleDelete=this.handleDelete.bind(this);
    
    this.handleStartDateChange=this.handleStartDateChange.bind(this);
    this.handleEndDateChange=this.handleEndDateChange.bind(this);
    this.handleOrganizationChange=this.handleOrganizationChange.bind(this);
    this.handleRegionChange=this.handleRegionChange.bind(this);
    this.handleSiteChange=this.handleSiteChange.bind(this);
    this.handleActivityScopeChange=this.handleActivityScopeChange.bind(this);
    this.handlePositionChange=this.handlePositionChange.bind(this);
    this.handleDetailsChange=this.handleDetailsChange.bind(this);    
    
  }
  
	componentWillMounted() {	 
	}
  
  	componentDidMount() {
  		var apiBaseUrl = process.env.REACT_APP_API_URL;
  		fetch(apiBaseUrl+'/employee/experience/'+this.props.employee.id)
  			.then(res => res.json())
  			.then(
  					(result) => {
  						this.setState({experiences: result});
  					},
  					(error) => {
  						console.log(error);
  					}
  			);
  	}  
   
  	handleAdd(event) {
  	  var newId = -1;
  	  var empexp = [];
  	  empexp = this.state.experiences.slice();
  	  if(empexp.length > 0) {
  		  var minId = empexp.reduce(function (p, v) {
  			    return ( p.id < v.id ? p.id : v.id );
  		  });
  		  if(minId < 0) {
  			  newId = minId - 1;
  		  }
  	  }
  	  empexp.push({
  		  id:newId,
  		  employee:this.props.employee,
  		  startDate:null,
  		  endDate:null,
  		  organization:'',
  		  region:'',
  		  site:'',
  		  activityScope:'',
  		  position:'',
  		  details:''
  	  });
  	  this.setState({experiences:empexp});
  	  this.props.onChange(empexp);
    }
  
  	handleDelete(event, currentEmployeeExperience) {
  	  var empexp = [];
  	  empexp = this.state.experiences.slice();
  	  var index = -1
  	  for(let i=0; i<empexp.length; i++) {
  		  if(empexp[i].id == currentEmployeeExperience.id) {
  			  index = i;
  			  break;
  		  }
  	  } 
  	  empexp.splice(index, 1);
  	  this.setState({experiences:empexp});
  	  this.props.onChange(empexp);
    }
  
  	handleStartDateChange(date, employeeExperience) {
	  if(date != null) {
  		  var empexp = [];
		  empexp = this.state.experiences.slice();
		  for(let i=0; i<empexp.length; i++) {
			  if(empexp[i].id == employeeExperience.id) {
				  empexp[i].startDate = date;
				  this.setState({experiences:empexp});
				  this.props.onChange(empexp);
				  break;
			  }
		  }
  	}
  }
  
  handleEndDateChange(date, employeeExperience) {
	  if(date != null) {
		  var empexp = [];
		  empexp = this.state.experiences.slice();
		  for(let i=0; i<empexp.length; i++) {
			  if(empexp[i].id == employeeExperience.id) {
				  empexp[i].endDate = date;
				  this.setState({experiences:empexp});
				  this.props.onChange(empexp);
				  break;
			  }
		  }
	  }
  }
  
  handleOrganizationChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].organization = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  
  handleRegionChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].region = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  
  handleSiteChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].site = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  

  handleActivityScopeChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].activityScope = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  
  handlePositionChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].position = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  
  
  handleDetailsChange(event, employeeExperience) {
	  var empexp = [];
	  empexp = this.state.experiences.slice();
	  for(let i=0; i<empexp.length; i++) {
		  if(empexp[i].id == employeeExperience.id) {
			  empexp[i].details = event.target.value;
			  this.setState({experiences:empexp});
			  this.props.onChange(empexp);
			  break;
		  }
	  }
  }
  
  render() {
    return (
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>  
    				<Grid item xs={9}>
    				</Grid>
    				<Grid item xs={3}>
    					<Button
		            		type="submit"
		            		fullWidth
		            		variant="contained"
		            		color="primary"  
		            		onClick={(event) => this.handleAdd(event, this.props.role)}
		            	>
    						{i18n.t("resume.addexperience.button.label")}
    					</Button>
	    			</Grid>
	    		
	    			<Grid item xs={12}>
	    		{
	    			this.state.experiences.map(item => (
        			   <Box>
	        			   <Grid container spacing={2}>
		        			   <Grid item xs={3}>
				        			<TextField
					    		        id={"experienceStartDatePicker" + item.id}
					    		        label={i18n.t("resume.experience.startdate")}
					    		        type="date"
					    		        
					    		        InputLabelProps={{
					    		          shrink: true,
					    		        }}
				    					value={item.startDate}
				    					onChange={(event) => this.handleStartDateChange(event, item)}
				    		        />
		        			   
				        			<TextField
					    		        id={"experienceFinishDatePicker" + item.id}
					    		        label={i18n.t("resume.experience.enddate")}
					    		        type="date"
					    		        
					    		        InputLabelProps={{
					    		          shrink: true,
					    		        }}
				    					value={item.endDate}
				    					onChange={(event) => this.handleEndDateChange(event, item)}
				    		        />
		        			   </Grid>	
				        			
		              		   <Grid item xs={3}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"organization" + item.id}
							            label={i18n.t("resume.experience.organization.label")}
							            name={"organization" + item.id}
							            autoFocus
							            value={item.organization}
							            onChange={(event) => this.handleOrganizationChange(event, item)}
				    				/>
		              		   </Grid>
				              	
				              	<Grid item xs={3}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"region" + item.id}
							            label={i18n.t("resume.experience.region.label")}
							            name={"region" + item.id}
							            autoFocus
							            value={item.region}
							            onChange={(event) => this.handleRegionChange(event, item)}
				    				/>
		              		   </Grid>		 
				              	
				              	<Grid item xs={2}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"site" + item.id}
							            label={i18n.t("resume.experience.site.label")}
							            name={"site" + item.id}
							            autoFocus
							            value={item.site}
							            onChange={(event) => this.handleSiteChange(event, item)}
				    				/>
		              		   </Grid>	
				              		 
				               <Grid item xs={1}>
			              			<IconButton aria-label="delete" onClick={(event => this.handleDelete(event, item))}>
			              				<DeleteIcon />
			              			</IconButton>
	              			   </Grid>

				               <Grid item xs={3}>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"position" + item.id}
							            label={i18n.t("resume.experience.position.label")}
							            name={"position" + item.id}
							            autoFocus
							            value={item.position}
							            onChange={(event) => this.handlePositionChange(event, item)}
				    				/>
		              		   </Grid>		 
				              		 
				              <Grid item xs={9} justify='space-around' alignItems='stretch'>
				              		 <TextField
							            variant="outlined"
							            margin="normal"
							            required
							            fullWidth
							            id={"activityscope" + item.id}
							            label={i18n.t("resume.experience.activityscope.label")}
							            name={"activityscope" + item.id}
							            autoFocus
							            value={item.activityScope}
							            onChange={(event) => this.handleActivityScopeChange(event, item)}
				    				/>
		              		   </Grid>	
				              		
		              		   <Grid item xs={12} justify='space-around' alignItems='stretch'>
				              		 <TextField
					                     id="outlined-multiline-static"
					                     label={i18n.t("resume.projectdescription.placeholder")}
					                     multiline
					                     fullWidth
					                     rows={2}
					                     variant="outlined"
					                     value={item.details}	 
				              		 	 onChange={(event => this.handleDetailsChange(event, item))}	 
				                    />
		        	          </Grid>
				              
	        			   </Grid>
	        			 </Box>
        			   
        	   ))}
           </Grid>
           </Grid>
           </div>
      </Container>
    		
    );
  }
}

export default EmployeeExperience;
