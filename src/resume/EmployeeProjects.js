import React, { Component } from 'react';
import Box from '@material-ui/core/Box';
import 'date-fns';
import ProjectSelect from '../select/ProjectSelect';
import RoleSelect from '../select/RoleSelect';
import i18n from 'i18next';
import DeleteIcon from '@material-ui/icons/Delete';
import Container from '@material-ui/core/Container';
import CssBaseline from '@material-ui/core/CssBaseline';
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import TextField from '@material-ui/core/TextField';

const style = {
  margin: 15,
};

class EmployeeProjects extends Component {
	
  constructor(props){
    super(props);
    this.state={
      employeeProjects:[]
    }
    
    this.handleAdd=this.handleAdd.bind(this);
    this.handleDelete=this.handleDelete.bind(this);
    
    this.handleProjectSelect=this.handleProjectSelect.bind(this);
    this.handleRoleSelect=this.handleRoleSelect.bind(this);
    this.handleDescriptionChange=this.handleDescriptionChange.bind(this);
    
  }
  
  componentDidMount() {
	  var apiBaseUrl = process.env.REACT_APP_API_URL;
	    fetch(apiBaseUrl+'/employee/projects/'+this.props.employee.id)
	      .then(res => res.json())
	      .then(
	        (result) => {
	        	
	        	this.setState(
	        			{employeeProjects: result}
	        	);
	        	
	        },
	        (error) => {
	        	console.log(error);
	        }
	      );
  }
  
  handleAdd(event) {
	  var newId = -1;
	  var empprj = [];
	  empprj = this.state.employeeProjects.slice();
	  if(empprj.length > 0) {
		  var minId = empprj.reduce(function (p, v) {
			    return ( p.id < v.id ? p.id : v.id );
		  });
		  if(minId < 0) {
			  newId = minId - 1;
		  }
	  }
	  empprj.push({
		  id:newId,
		  employee:this.state.employee ,
		  project:{
			  id:null,
			  name:'',
			  startDate:null,
			  finishDate:null,
			  description:''
		  },
		  role:{
			  id:null,
			  name:''
		  },
		  details:''
	  });
	  this.setState({employeeProjects:empprj});
	  this.props.onChange(empprj);
  } 
  
  handleDelete(event, currentEmployeeProject) {
	  var empprj = [];
	  empprj = this.state.employeeProjects.slice();
	  var index = -1
	  for(let i=0; i<empprj.length; i++) {
		  if(empprj[i].id == currentEmployeeProject.id) {
			  index = i;
			  break;
		  }
	  } 
	  empprj.splice(index, 1);
	  this.setState({employeeProjects:empprj});
	  this.props.onChange(empprj);
  }
  
  handleProjectSelect(newProject, employeeProjectId) {
	  var empprj = [];
	  empprj = this.state.employeeProjects.slice();
	  for(let i=0; i<empprj.length; i++) {
		  if(empprj[i].id == employeeProjectId) {
			  empprj[i].project = newProject;
			  this.setState({employeeProjects:empprj});
		  }
	  } 
	  this.props.onChange(empprj);
  }
  
  handleRoleSelect(newRole, employeeProjectId) {
	  var empprj = [];
	  empprj = this.state.employeeProjects.slice();
	  for(let i=0; i<empprj.length; i++) {
		  if(empprj[i].id == employeeProjectId) {
			  empprj[i].role = newRole;
			  this.setState({employeeProjects:empprj});
		  }
	  }
	  this.props.onChange(empprj);
  }
  
  handleDescriptionChange(event, employeeProject) {
	  var empprj = [];
	  empprj = this.state.employeeProjects.slice();
	  for(let i=0; i<empprj.length; i++) {
		  if(empprj[i].id == employeeProject.id) {
			  empprj[i].details = event.target.value;
			  this.setState({employeeProjects:empprj});
		  }
	  }
	  this.props.onChange(empprj);
  }
    
  render() {
    return (
    		<Container component="main">
    		<CssBaseline />
    		<div>
    			<Grid container spacing={2}>  
    				<Grid item xs={9}>
    				</Grid>
    				<Grid item xs={3}>
    					<Button
		            		type="submit"
		            		fullWidth
		            		variant="contained"
		            		color="primary"  
		            		onClick={(event) => this.handleAdd(event, this.props.role)}
		            	>
    						{i18n.t("resume.addproject.button.label")}
    					</Button>
	    			</Grid>
	    			<Grid item xs={12}>
	    		{
	    			this.state.employeeProjects.map(item => (
        			   <Box>
	        			   <Grid container spacing={2}>
		        			   <Grid item xs={3}>
		        			   		<ProjectSelect 	
			        			   		label={i18n.t("resume.project.select.label")}  	
			        			   		value={item.project.id} 
			        			   		objectId={item.id} 
			        			   		onChange={this.handleProjectSelect} 
			        			   		helplabel={(item.project.startDate == null ? "" : item.project.startDate) + ' - ' + (item.project.finishDate == null ? "" : item.project.finishDate)}
		        			   		/>
		        			   </Grid>
		              		   <Grid item xs={3}>
		              				<RoleSelect 	label={i18n.t("resume.role.select.label")} 		value={item.role.id} 	objectId={item.id} onChange={this.handleRoleSelect} />
		              		   </Grid>
		              		   
		              		   <Grid item xs={5} justify='space-around' alignItems='stretch'>
				              		 <TextField
					                     id="outlined-multiline-static"
					                     label={i18n.t("resume.projectdescription.placeholder")}
					                     multiline
					                     fullWidth
					                     rows={2}
					                     variant="outlined"
					                     value={item.details}	 
				              		 	 onChange={(event => this.handleDescriptionChange(event, item))}	 
				                    />
		        	          </Grid>
				              <Grid item xs={1}>
			              			<IconButton aria-label="delete" onClick={(event => this.handleDelete(event, item))}>
			              				<DeleteIcon />
			              			</IconButton>
	              			   </Grid>
	        			   </Grid>
	        			 </Box>
        			   
        	   ))}
           </Grid>
           </Grid>
           </div>
      </Container>
    );
  }
}

export default EmployeeProjects;
