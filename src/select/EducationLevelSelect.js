import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class EducationLevelSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      levels:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/education/level/all')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({levels: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.levels.length; j++) {
			if(this.state.levels[j].id == event.target.value) {
				this.props.onChange(this.state.levels[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.levels} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default EducationLevelSelect;