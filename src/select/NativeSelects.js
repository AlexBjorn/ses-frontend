import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import FormHelperText from '@material-ui/core/FormHelperText';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';

const useStyles = makeStyles((theme) => ({
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
  },
  selectEmpty: {
    marginTop: theme.spacing(2),
  },
}));

export default function NativeSelects(props) {
  const classes = useStyles();

  return (
	      <FormControl variant="outlined" className={classes.formControl}>
	        <InputLabel id="demo-simple-select-placeholder-label-label">
	          {props.label}
	        </InputLabel>
	        <Select
	          labelId="demo-simple-select-placeholder-label-label"
	          id="demo-simple-select-placeholder-label"
	          value={props.value == null ? "":props.value}
	          onChange={props.onChange}
	          displayEmpty
	          className={classes.selectEmpty}
	        >
	          <MenuItem value="">
	            <em></em>
	          </MenuItem>
	          {
	        		props.options.map(
	        				opt => (
	        						<MenuItem value={opt.id}>{opt.name}</MenuItem>
	        				)
	        		)
	        	}
	        </Select>
	        <FormHelperText>{props.helplabel}</FormHelperText>
	      </FormControl>
	 );
}
