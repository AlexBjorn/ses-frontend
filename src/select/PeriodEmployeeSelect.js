import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class PeriodEmployeeSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      employees:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/valuation/period/' + this.props.period.id + '/employee/' + this.props.user.employee.id + '/coemployees')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({employees: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.employees.length; j++) {
			if(this.state.employees[j].id == event.target.value) {
				this.props.onChange(this.state.employees[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.employees} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default PeriodEmployeeSelect;