import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class ProjectSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      projects:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/project/all')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({projects: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.projects.length; j++) {
			if(this.state.projects[j].id == event.target.value) {
				this.props.onChange(this.state.projects[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.projects} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default ProjectSelect;