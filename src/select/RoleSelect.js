import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class RoleSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      roles:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/role/all')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({roles: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.roles.length; j++) {
			if(this.state.roles[j].id == event.target.value) {
				this.props.onChange(this.state.roles[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.roles} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default RoleSelect;