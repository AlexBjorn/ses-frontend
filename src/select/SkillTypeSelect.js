import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class SkillTypeSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      skillTypes:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/skill/type/all')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({skillTypes: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.skillTypes.length; j++) {
			if(this.state.skillTypes[j].id == event.target.value) {
				this.props.onChange(this.state.skillTypes[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.skillTypes} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default SkillTypeSelect;