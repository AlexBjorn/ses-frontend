import React, { Component } from 'react';
import NativeSelects from './NativeSelects';

class ValuationPeriodSelect extends Component {
	
	constructor(props){
	    super(props);
	    this.state={
	      valuationPeriods:[]
	    }
	    this.onChange = this.onChange.bind(this);
	}
	    
	componentDidMount() {
		var apiBaseUrl = process.env.REACT_APP_API_URL;
		fetch(apiBaseUrl+'/valuation/period/all')
			.then(res => res.json())
			.then(
				(result) => {
					this.setState({valuationPeriods: result});
				},
				(error) => {
					console.log(error);
				}
			);
	}
	
	onChange(event) {
		for(let j=0; j<this.state.valuationPeriods.length; j++) {
			if(this.state.valuationPeriods[j].id == event.target.value) {
				this.props.onChange(this.state.valuationPeriods[j], this.props.objectId);
			}
		}
	}
	
	render() {
		
	    return (
        	<NativeSelects 
        		label={this.props.label} 
        		helplabel={this.props.helplabel} 
        		value={this.props.value} 
        		options={this.state.valuationPeriods} 
        		onChange={event=>this.onChange(event)} 
        	/>
	    );
	}
	
}

export default ValuationPeriodSelect;